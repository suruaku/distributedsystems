"use strict";
/**
 * Program serializes and deserializes data.
 * The result is updated to google sheet.
 * Google API related functions are borrowed
 * from Google API docs and modified accordingly.
 */
const fs                = require( 'fs' );
const readline          = require( 'readline' );
const { performance }   = require( 'perf_hooks' );

const config            = require( 'config' );
const msgpack           = require( 'msgpack' );
const xml2js            = require( 'xml2js' );
const yaml              = require( 'js-yaml' );
const { google }        = require( 'googleapis' );


// If modifying these scopes, delete token.json.
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const SCOPES = [ "https://www.googleapis.com/auth/drive" ];
const TOKEN_PATH = 'token.json';
const CYCLE = 1000;
const SHEET = config.get( 'spreadsheetId' );

/******************** JSON ********************/
const dJson1 = () => {
    let i;
    
    const data = fs.readFileSync( __dirname + '/foo.json', function( err, data ) {
        return data
    });

    const t0 = performance.now();
    for ( i=0; i < CYCLE; i++ ){
        JSON.parse( data );
    };

    const t1 = performance.now();
    return Math.round(( t1 - t0 ) * 100 ) / 100;
};

const sJson1 = () => {
    let i;
    
    const data = fs.readFileSync( __dirname + '/foo.json', function( err, data ) {
        return data
    });

    const obj = JSON.parse( data );

    const t0 = performance.now();
    for ( i=0; i < CYCLE; i++ ){
        JSON.stringify( obj );
    };

    const t1 = performance.now();
    return Math.round(( t1 - t0 ) * 100 ) / 100;
};
/******************** !JSON *******************/


/******************** MESSAGEPACK *******************/
const dMsgPack2 = () => {
    let i;

    const data = fs.readFileSync( __dirname + '/foo.json', function( err, data ) {
        return data
    });

    const t0 = performance.now();
    for ( i=0; i < CYCLE; i++ ){
        msgpack.unpack( data );
    };
    const t1 = performance.now();

    return Math.round(( t1 - t0 ) * 100 ) / 100;
};

const sMsgPack2 = () => {
    let i;

    const data = fs.readFileSync( __dirname + '/foo.json', function( err, data ) {
        return data
    });

    const obj = msgpack.unpack( data );

    const t0 = performance.now();
    for ( i=0; i < CYCLE; i++ ){
        msgpack.pack( obj );
    };
    const t1 = performance.now();

    return Math.round(( t1 - t0) * 100 ) / 100;
};
/******************** !MESSAGEPACK ******************/


/******************** XML *******************/
const dXml3 = () => {
    const parser = new xml2js.Parser();
    let i;

    const data = fs.readFileSync( __dirname + '/foo.xml', function( err, data ) {
        return data
    });

    const t0 = performance.now();
    for ( i=0; i < CYCLE; i++ ){
            parser.parseString( data, ( res, err ) => {
        });
    }
    const t1 = performance.now();

    return Math.round(( t1 - t0 ) * 100 ) / 100;
};

const sXml3 = () => {
    const builder = new xml2js.Builder();
    let i;

    const data = fs.readFileSync( __dirname + '/foo.xml', function( err, data ) {
        return data
    });

    const obj = xml2js.parseStringPromise( data ).then(( obj ) => {
        return obj;
    });

    const t0 = performance.now();
    for ( i=0; i < CYCLE; i++ ){
            builder.buildObject( obj );
    }
    const t1 = performance.now();

    return Math.round(( t1 - t0 ) * 100 ) / 100;
};
/******************** !XML ******************/


/******************** YML ******************/
const dYaml4 = () => {
    let i;
    const data = fs.readFileSync( __dirname + '/foo.yml', 'utf8' );

    const t0 = performance.now();
    for ( i=0; i < CYCLE; i++ ){
        yaml.safeLoad( data );
    }
    const t1 = performance.now();

    return Math.round(( t1 - t0 ) * 100 ) / 100;
};

const sYaml4 = () => {
    let i;
    const data = fs.readFileSync( __dirname + '/foo.yml', 'utf8' );

    const obj = yaml.safeLoad( data );

    const t0 = performance.now();
    for ( i=0; i < CYCLE; i++ ){
        yaml.safeDump( obj );
    }
    const t1 = performance.now();

    return Math.round(( t1 - t0 ) * 100 ) / 100;
};
/******************** !YML*****************/


/***************** GOOGLE CODE *************/
// Load client secrets from a local file.
fs.readFile('credentials.json', (err, content) => {
  if (err) return console.log('Error loading client secret file:', err);
  // Authorize a client with credentials, then call the Google Sheets API.
  authorize(JSON.parse(content), updateFields);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error while trying to retrieve access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}
/***************** !GOOGLE CODE ************/


function updateFields( auth ) {
    const values = [
        [ dJson1(), dMsgPack2(), dXml3(), dYaml4() ],
        [ sJson1(), sMsgPack2(), sXml3(), sYaml4() ],
    ];

    const data = [{
        range: 'B2:E3',
        values
    }];

    const sheets = google.sheets({ version: 'v4', auth });
    sheets.spreadsheets.values.batchUpdate({
        "spreadsheetId": SHEET,
        resource: {
            data,
            valueInputOption: "USER_ENTERED"
        }
    }, ( err, res ) => {
        if ( err ) return console.log( 'The API returned an error: ' + err );
    });
};