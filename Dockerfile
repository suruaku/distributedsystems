FROM ubuntu:18.04

WORKDIR /distro

RUN apt update && apt upgrade -y

RUN apt install -y --no-install-recommends curl

RUN curl --silent --location https://deb.nodesource.com/setup_10.x | bash -

RUN apt install -y nodejs build-essential

COPY . .


