# Distributed systems course
Author: Ilja


## Docker
Everything of this project can be run inside docker container.
```
docker build -t distro . && docker run -it distro /bin/bash
```

## TCP-sockets
Server and client chat application using TCP sockets written in Python3.
The magic happens in the `select` module. `select.select()` listens for a range of file descriptors, when
file descriptor changes the program continues. `server.accept()` returns a new file descriptor
out of a new socket connection. This behavior can be tracked with a running server as follows. It will
print list of current file descriptors, changes in them et cetera.
```
strace ./server.py
```

`select` module is an interface for Unix `select()` function which keeps track of changes in given range of file
descriptors.

### Sources
* [Sockets Tutorial with Python 3 part 1 - sending and receiving data](https://pythonprogramming.net/sockets-tutorial-python-3/)
* [Async IO on Linux: select, poll, and epoll](https://jvns.ca/blog/2017/06/03/async-io-on-linux--select--poll--and-epoll/)
* [select — Waiting for I/O completion](https://docs.python.org/3/library/select.html)


## Serialize
Serialization and deserialization of JSON, XML and YAML. Using NodeJS.
Results are updated to Google Sheet after each run of the program.
Change API key for the upload to work.


## wikicrawler
Wikipedia crawler to find shortest path between two pages. Uses `threading` and `queue` build-in libraries.
