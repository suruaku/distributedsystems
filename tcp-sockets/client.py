#!/usr/bin/env python3
import configparser
import errno
import socket
import sys
import time


# Set up config
config = configparser.ConfigParser()
config.read('config.conf')

# Globals
try:
    TCP_IP = config['DEFAULT']['TCP_IP']
except KeyError:
    TCP_IP = 'kotir.space'

try:
    TCP_PORT = int( config['DEFAULT']['TCP_PORT'] )
except KeyError:
    TCP_PORT = 5000

try:
    HEADER_SIZE = int( config['DEFAULT']['HEADER_SIZE'] )
except KeyError:
    HEADER_SIZE = 10


def info():
    print('''
░█████╗░██╗░░██╗░█████╗░████████╗
██╔══██╗██║░░██║██╔══██╗╚══██╔══╝
██║░░╚═╝███████║███████║░░░██║░░░
██║░░██╗██╔══██║██╔══██║░░░██║░░░
╚█████╔╝██║░░██║██║░░██║░░░██║░░░
░╚════╝░╚═╝░░╚═╝╚═╝░░╚═╝░░░╚═╝░░░\n''')
    print('Communication Handeld by A Tcp\n')
    print('To quit write only "q" in the message and press ENTER.\n')
    print('To update chat(i.e. receive new messages) just press ENTER.')


def get_username():
    while True:
        username = input('Enter your username: ')
        if username:
            return username


def main():
    #TODO change encode to b-string
    client_username = get_username()  

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        client.connect((TCP_IP, TCP_PORT))
        print(f'Connected to { TCP_IP }\n')
        client.setblocking(False)

        header_username = f'{len( client_username ):<{ HEADER_SIZE }}'.encode('utf-8')
        client.send(header_username + client_username.encode('utf-8'))

        while True:
            timestamp = time.strftime("%H:%M", time.gmtime())
            timestamp = f'{timestamp:<5}'
            message_send = input(f'[{timestamp} {client_username}]> ')

            if message_send == 'q':
                print('Closing the connection.')
                client.close()
                sys.exit(0)
            elif message_send:  #Send message
                timestamp = time.strftime("%H:%M", time.gmtime())
                message_send += f'{timestamp:<5}'
                message_send = message_send.encode('utf-8')
                header_message = f'{len( message_send ):<{ HEADER_SIZE }}'.encode('utf-8')
                client.send(header_message + message_send)
            try:
                while True:  #Receive message
                    header_username = client.recv(HEADER_SIZE)
                    if not len(header_username):
                        print('** Server closed connection. **')
                        sys.exit(0)
                    username_length = int(header_username.decode('utf-8').strip())
                    username = client.recv(username_length).decode('utf-8')

                    header_message = client.recv(HEADER_SIZE)
                    message_length = int(header_message.decode('utf-8').strip())
                    message_recv = client.recv(message_length).decode('utf-8')
                    timestamp = message_recv[-5:]
                    print(f'[{timestamp} {username}]> {message_recv[:-5]}')
            except IOError as e:
                if e.errno != errno.EAGAIN and e.errno != errno.EWOULDBLOCK:
                    print(str(e))
                    sys.exit(1)
                continue
            except Exception as e:
                print(str(e))
                sys.exit(1)


if __name__ == '__main__':
    info()
    main()
