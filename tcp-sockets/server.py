#!/usr/bin/env python3 import configparser
import socket
import select
import time

### WHY TCP
# Reliable delivery. Important for chat
# Guaranteed correct ordering of packets.
# 


# Set up config
config = configparser.ConfigParser()
config.read('config.conf')

# Globals
try:
    TCP_IP = config['DEFAULT']['TCP_IP']
except KeyError:
    TCP_IP = socket.gethostname()

try:
    TCP_PORT = int( config['DEFAULT']['TCP_PORT'] )
except KeyError:
    TCP_PORT = 5000

try:
    HEADER_SIZE = int( config['DEFAULT']['HEADER_SIZE'] )
except KeyError:
    HEADER_SIZE = 10

# Notify all users
def broadcast():
    pass


def receive_message(client_socket):
    try:
        header_message = client_socket.recv(HEADER_SIZE)
        if not len(header_message):
            return False
        message_length = int(header_message.decode('utf-8').strip())
        return { 'header': header_message, 'data': client_socket.recv(message_length) }
    except:
        return False


def main():  # IPV4 & TCP
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  #Errno 98
        server.bind((TCP_IP, TCP_PORT))
        server.listen()
        print(server)
        print(f'Listening for connections on { TCP_IP }:{ TCP_PORT }')
        clients = {}
        sockets_list = [server]
        while True:  # Main loop
            read_sockets, _, exception_sockets = select.select(sockets_list, [], sockets_list)
            for notified_socket in read_sockets:
                if notified_socket == server:  # New connection
                    client_socket, client_address = server.accept()
                    print(client_socket)
                    print('\n\n')
                    print(client_address)
                    user = receive_message(client_socket)
                    if user is False:
                        continue
                    sockets_list.append(client_socket)
                    clients[client_socket] = user
                    username = user['data'].decode('utf-8')
                    print('Accepted new connection from {}:{}, username: {}'.format(*client_address, username))
                else:  # New message
                    message = receive_message(notified_socket)
                    if message is False:  # Disconnected client
                        print('Closed connection from: {}'.format(clients[notified_socket]['data'].decode('utf-8')))
                        sockets_list.remove(notified_socket)
                        del clients[notified_socket]
                        continue
                    user = clients[notified_socket]
                    print(f'Received message from {user["data"].decode("utf-8")}: {message["data"].decode("utf-8")}')
                    # Send the message to connected clients
                    for client in clients:
                        if client != notified_socket:
                            client.send(user['header'] + user['data'] + message['header'] + message['data'])
            # Remove exceptinal clients
            for notified_socket in exception_sockets:
                sockets_list.remove(notified_socket)
                del clients[notified_socket]


if __name__ == '__main__':
    main()
