#!/usr/bin/env python3
import time
import urllib
from queue import Queue
from threading import Thread
import sys

import bs4
import requests

depth_list = []

def verify_link(link):
    if link.get('href') and link.get('href')[:6] == "/wiki/":
        if (link.contents and str(link.contents[0])[0] != "<"
                and ":" not in link.get('href')):
            return True
    return False

def find_links(query):
    depth = 0
    links = []
    url = f"https://en.wikipedia.org/wiki/{query}"
    time.sleep(1)
    res = requests.get(url)
    html = res.text
    soup = bs4.BeautifulSoup(html, "html.parser").find(
        "div", {"id": "mw-content-text"})

    # remove some sections
    for div in soup.find_all('div', {'class': 'reflist'}):
        div.decompose()
    for div in soup.find_all('div', {'class': 'navbox'}):
        div.decompose()
    for div in soup.find_all('div', {'class': 'refbegin'}):
        div.decompose()
    for div in soup.find_all('div', {'id': 'mw-panel'}):
        div.decompose()
    ##
    for paragraph in soup.find_all("p"):
        for link in paragraph.find_all('a'):
            if verify_link(link):
                links.append(link)
    for paragraph in soup.find_all("ul"):
        for link in paragraph.find_all('a'):
            if verify_link(link):
                links.append(link)
    return [(a.get('href')[:6], a.contents[0], depth) for a in links]

def main():
    query = "Cat"
    result = 'Santa'
    depth = 0
    links = find_links(query)
    matriisi = []
    while True:
        if depth > 7:
            print('Too deep.')
            sys.exit(0)
        for i, link in enumerate(links):
            if link[1] == result:
                print(link[1])
                sys.exit(0)
            print(link[1])
            matriisi[depth][i] = find_links(link[i][1])
        depth += 1
    print('yes')

if __name__ == '__main__':
    main()
